from django.shortcuts import render, redirect
from .models import Person
from .forms import PersonForm



# Create your views here.

def persons_list(request):
    persons = Person.objects.all()

    return render(request, 'person_list.html',{'persons': persons})

def persons_create(request):
    form = PersonForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('person_list')

    return render(request, 'person_create.html', {'form':form})
