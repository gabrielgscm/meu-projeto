from django.urls import path
from .views import persons_list, persons_create

urlpatterns = [
    path('list/', persons_list, name='person_list'),
    path('create/', persons_create, name='person_create')
]